var fs = require('fs')
var express = require('express');
var _ = require('underscore');
var app = express();

app.use('/',express.static(__dirname + '/public'));

var people;
fs.readFile('./middle_ages.txt',function(err,data){
  people = _.map(data.toString().split('\n'),function(row){
    var person = {};
    var words = row.split('\t');
    person.name = words[0];
    person.role = words[1];
    person.place = words[2];
    person.birth = words[3];
    person.death = words[4];
    return person;
  })
});

app.get('/people',function(req,res){
  var role = req.query['role'];
  var matchedPeople = _.filter(people,function(person){
    return person['role'] == role;
  })
  res.send(matchedPeople);
})

app.listen(7271,function(){
	console.log('server@7271');
});
